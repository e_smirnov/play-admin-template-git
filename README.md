# Play Admin Template Web Application

This repository is a template web application, created with Play! Framework 1.4.x.

It contains a stub for a web admin with following components:

1. Backend with controllers, models and other stuff as defined in Play!
2. Simple user management: model class for User, CRUD actions for users, changing
user passwords. 
3. Simple frontend created with JQuery + Bootstrap 3, using free SBAdmin theme

All web admin tools I created started with these 3 basic components, so to save myself 
some time I decided to create this template. You can simply clone it, change some properties
(like app name and db connection details) located in application.conf (read Play Framework guides
about how to configure them) and then start extending it with your own admin features.

## Building and running

### Prerequisites

This template app requires:

1. Java 8+
2. Play Framework 1.4+. Please be sure that you have Play version 1. Play Framework version 2.x is a totally
different framework that has little in common with 1.x branch, they are totally incompatible with each other.
3. Some database (by default this app uses MySQL, but you may configure any DB you like via JDBC) 

### Running the app

1. Download Play Framework distribution from its official site if you do not have it. Add it to you executable path so that 
you are able to execute its main 'play' command
2. Go to the play-admin-template directory
3. Run 'play dependencies' command that will download and install necessary framework dependencies
4. Update the application.conf file with your own DB URL
5. Run 'play run'. After server is started your app will be available by default at http://localhost:9000

### Default Login

During first app start a user 'admin' with password '12345' will be created.
*Do not forget to change the password to something more secure!*

### App distribution

Play applications are intended to be run by play CLI like shown above. However there is a way to build a fat war that 
will contain both app code and all framework dependencies. This war file can later be deployed to any servlet container like Tomcat.
Consult Play documentation to learn how to do that.

## License

App and its source code are distributed under the terms of the MIT license. You may freely use this app or its parts
in commercial closed-source applications.