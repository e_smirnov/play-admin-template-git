package models;

import play.data.validation.Required;
import play.db.jpa.GenericModel;
import play.db.jpa.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.security.SecureRandom;
import controllers.Security;

@Entity
public class User extends GenericModel  {
    @Id
    public String login;

    public long registrationDate;

    public byte[] passwordHash;

    public byte[] passwordSalt;

    public User(String login, String password) {
        this.login = login;
        setPassword(password);
        registrationDate = System.currentTimeMillis();
    }

    @Override
    public String toString() {
        return login;
    }

    public void setPassword(String password) {
        if (password.isEmpty()) {
            // do not save empty passwords
            return;
        }
        passwordSalt = new byte[32];
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextBytes(passwordSalt);
        passwordHash = Security.calcHash(password, passwordSalt);
    }

    @Override
    public Object _key() {
        return login;
    }
}
