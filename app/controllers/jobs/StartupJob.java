package controllers.jobs;

import controllers.Version;
import models.User;
import org.apache.log4j.spi.RootLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.Play;
import play.jobs.Job;
import play.jobs.OnApplicationStart;

@OnApplicationStart
public class StartupJob extends Job {

    private static final Logger logger = LoggerFactory.getLogger(StartupJob.class);

    private static final String defaultAdminUsername = "admin";

    private static final String defaultAdminPassword = "12345";

    @Override
    public void doJob() {

        logger.info("Starting up " + Play.configuration.getProperty("application.name") + " v{}...", Version.VERSION);

        // Check if the database is empty
        if (User.count() == 0) {
            // add superadmin user
            User user = new User(defaultAdminUsername, defaultAdminPassword);
            user.save();
        }
        
        logger.info("Startup completed");
    }
}
