package controllers;

import models.User;
import play.Logger;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.i18n.Messages;
import play.modules.paginate.ModelPaginator;
import play.mvc.Controller;
import play.mvc.With;

@With(Secure.class)
public class Users extends Controller {

    private static User checkAndFindUser(String login) {
        User user = User.findById(login);
        if (user == null) {
            flash.error(Messages.get("user.notFound"));
            list();
        }

        return user;
    }

    private static boolean checkPasswordsMatch(String password, String passwordRepeat, String validationKey) {
        if (!password.equals(passwordRepeat)) {
            Logger.warn("Passwords do not match");
            params.flash();
            flash.error(Messages.get("user.passwordMismatch"));
            Validation.addError(validationKey, Messages.get("user.passwordMismatch"));
            Validation.keep();
            return false;
        }
        return true;
    }

    public static void list() {
        ModelPaginator<User> paginator = new ModelPaginator<User>(User.class);
        render(paginator);
    }

    public static void create() {
        render();
    }

    public static void edit(@Required String login) {
        User user = checkAndFindUser(login);
        render(user);
    }

    public static void delete(@Required String login) {
        User user = checkAndFindUser(login);
        if (User.count() == 1) {
            Logger.warn("Can not delete last user");
            flash.error(Messages.get("user.cantDeleteLast"));
            list();
        }
        user.delete();
        flash.success(Messages.get("user.deleted", user.login));
        list();
    }

    public static void createImpl(
            @Required String login,
            @Required String password,
            @Required String passwordRepeat
            ) {
        if (Validation.hasErrors()) {
            Logger.warn("Some errors in createImpl parameters");
            params.flash();
            Validation.keep();
            create();
        }

        User user = User.find("byLogin", login).first();
        if (user != null) {
            Logger.warn("User " + login + " already exists");
            flash.error(Messages.get("user.alreadyExists"));
            params.flash();
            Validation.addError("login", Messages.get("user.alreadyExists"));
            Validation.keep();
            create();
        }

        if (!checkPasswordsMatch(password, passwordRepeat, "passwordRepeat")) {
            create();
        }
        Logger.info("Creating user " + login);
        user = new User(login, password);
        user.save();
        flash.success(Messages.get("user.created"));
        list();
    }

    public static void editImpl(
            @Required String login,
            @Required String newPassword,
            @Required String newPasswordRepeat
    ) {
        if (Validation.hasErrors()) {
            Validation.keep();
            params.flash();
            edit(login);
        }
        User user = checkAndFindUser(login);
        if (!checkPasswordsMatch(newPassword, newPasswordRepeat, "newPasswordRepeat")) {
            edit(login);
        }

        Logger.info("Updating password for user " + login);
        user.setPassword(newPassword);
        user.save();
        flash.success(Messages.get("user.passwordUpdated"));
        list();
    }
}
